<nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
    <ul class="nav nav-pills flex-column">
        <li class="nav-item">
            @if (\Route::current()->getName() == 'home')
                <a class="nav-link active" href="javascript: void(0);">Manage Blogs <span class="sr-only">(current)</span></a>
            @else
                <a class="nav-link" href="{{ route("home") }}">Manage Blogs <span class="sr-only">(current)</span></a>
            @endif
        </li>
    </ul>
    <ul class="nav nav-pills flex-column">
        <li class="nav-item">
            <a class="nav-link" href="{{ route("index") }}">View Blogs</a>
        </li>
    </ul>
</nav>