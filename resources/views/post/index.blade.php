@extends('layouts.master')

@section('title')
    Welcome to the blog
@endsection

@section('content')
    <main role="main" class="container"  style="margin-top: 5px">

        <div class="row">

            <div class="col-sm-8 blog-main">

                @foreach($posts as $post)
                    <div class="blog-post">
                        <h2 class="blog-post-title">{{ $post->title }}</h2>
                        <p class="blog-post-meta"><small><i>{{ Carbon\Carbon::parse($post->created_at)->format('d-m-Y')  }} by
                                    @if(isset($user_dict[$post->author]))
                                        <a href="#">{{ $user_dict[$post->author] }}</a>
                                    @else
                                        <a href="#">{{ $post->author }}</a>
                                    @endif
                                </i>
                            </small>
                        </p>
                        <p>
                            <div>
                                {!! \Illuminate\Support\Str::words(strip_tags(html_entity_decode($post->description)), 30, '...') !!}
                            </div>
                        </p>
                        <blockquote>
                            <p>
                                <a href="{{ route('post.read', ['post_id' => $post->id]) }}" class="btn btn-primary btn-sm">Learn more</a>
                            </p>
                        </blockquote>
                    </div><!-- /.blog-post -->
                @endforeach

            </div><!-- /.blog-main -->

        </div><!-- /.row -->

    </main><!-- /.container -->
@endsection