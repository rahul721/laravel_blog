
@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">

            @include("partials.home_nav")

            <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
                <a href="{{route("home")}}"><span class="arrow-left"><< </span> Back</a>
                <h1>{{ $post->title }}</h1>
                <div class="col-sm-8 blog-main">
                    <div class="blog-description">
                        <?= $post->description ?>
                    </div>
                    <div class="button-area">
                        <a href="{{ route("post.edit", ["id" => $post->id]) }}">
                            <button type="button" class="btn btn-primary btn-sm">Edit Post</button>
                        </a>
                        <a href="{{ route("post.delete", ["id" => $post->id]) }}">
                            <button type="button" class="btn btn-danger btn-sm">Delete Post</button>
                        </a>
                    </div>
                </div>
            </main>
        </div>
    </div>
@endsection