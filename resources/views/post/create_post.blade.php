
@extends('layouts.master')

@section("scripts")
    <script>
        $(document).ready(function() {
            $("#create-blog").on("submit", function() {
                let title = $("#id_title").val();
                let editor_content = $("#editor").html().trim();
                console.log(editor_content);
                let notify = $(".notify-area #notify");
                let isValid =  true;
                let errorMessage =  "";
                if (title === "") {
                    isValid = false;
                    errorMessage = "Please enter a title for your blog.";
                }
                if (editor_content === "" || editor_content === "<p><br></p>") { // There is a gaping hole here,.. but oh well...
                    isValid = false;
                    errorMessage += (errorMessage === "") ? "Please enter the contents of your blog." : "<br />Don't forget to enter the contents of your blog as well.";
                }
                // either cleared or error message is populated...
                notify.html(errorMessage);
                if (isValid) {
                    $("#id_description").val(editor_content);
                    return true;
                }

                return false;
            });
        });
    </script>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">

            @include("partials.home_nav")

            <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
                <h1>Create Post</h1>
                <div class="col-md-4">
                    <form method="post" action="" id="create-blog">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Title</label>
                            <input type="text" class="form-control" id="id_title" name="title"
                                   aria-describedby="title" placeholder="Enter title">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <? // Hidden text area that will hold the editor contents if the form is valid ?>
                            <textarea class="form-control" id="id_description" rows="3" name="description" placeholder="Description" style="display: none;"></textarea>

                            @include("partials.editor", array("content" => "<p>{Time to get creative with your new blog...}</p>"))

                        </div>
                        <div class="button-area">
                            <button type="submit" class="btn btn-primary">Create Post</button>
                            <a href="{{ route("home") }}">
                                <button type="button" class="btn btn-danger">Cancel</button>
                            </a>
                        </div>
                        <div class="notify-area">
                            <span id="notify"></span>
                        </div>
                    </form>
                </div>
            </main>
        </div>
    </div>
@endsection