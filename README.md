# Excercise to Create a Blog using Laravel and MongoDB

A blog has to be created where a user can read posts without logging in. An admin however can login to read posts, create new posts, update and delete existing posts.

## Assumptions and considerations
1. All blogs are **visible to any user** who interacts with the system regardless of their login status.
2. **A user can login** to the blog after which they can create blogs and publish they in the application.
3. A logged in user can **update and/or delete** only the blogs that they create.
4. It was decided to not implement the app in VueJS due to time constraints.

---

## Platform
1. Windows 7 Home Premium Service Pack 1
2. PHP aritsan developmental web application server.

## Application Stack
1. Laravel 5.6.38 & dependancies
2. PHP7.2.0 (with php_mongodb.dll v1.5.3 x86 non thread safe)
3. MongoDB v4.0.2
4. MySQL 5.7.11

## Resources & tools
1. jquery 3.3.1
2. Bootstrap 4.1.3 (for UI styles)
3. fontawesome-free 5.3.1
4. WYSIWYG Editor [from here](https://codepen.io/Shokeen/pen/pgryyN) - for handling blog post content.
5. Tutorial - [Building a blog using Laravel](https://muva.co.ke/blog/lesson-1-introduction-building-blog-application-using-laravel-5-5-php-7-0/)
6. Jetbrains PhpStorm 2018.2.4 (IDE)
7. Composer 1.7.2

---

## Deployment instructions
Please ensure that you have all the components of the application stack in your computer. If any of the application stack or the tools mentioned in the below points are missing, please install the versions mentioned in the application stack or greater for your Operating system.

1. Ensure you have **php 7.20**; See [PHP 7.2 installation](http://php.net/manual/en/install.php).
2. Install **MySQL** in your system if not already available. See [Getting Started with MySQL](https://dev.mysql.com/doc/mysql-getting-started/en/).
3. Please follow the [Laravel Installation guide](https://laravel.com/docs/5.6/installation) and/or [mongoDB Installation guide](https://docs.mongodb.com/manual/administration/install-community/) either of these are not available in your computer.
4. Start the services for both **MySQL and MongoDB**.
5. Install **Composer** in your system. See [Composer - Getting Started](https://getcomposer.org/doc/00-intro.md).

Once you have the application stack in place:

1. **Clone the contents of this repository** to desired location of your computer.
2. To install all the **required dependencies**, run ``composer install``.
3. Ensure that the **Mysql database tables** are created. Follow [Database: Migrations](https://laravel.com/docs/5.7/migrations#creating-tables) for instructions.
4. **Start** your application server. If it is the Laravel Artisan server, run the command ``sh php artisan serve`` from your project directory.

---

## Using the App
Authentication and user management for this app is the **standard and simple authentication** offered by Laravel.
The application has a **top header bar** that indicates what type of user is interacting with the system.
At the right corner of this bar, A text with a **drop-down menu** reads 'Guest' for users who are not logged in to the application. Once a user logs in, the text changes to the user's name (that they supplied on sign up).

**The dropdown menu allows a guest user to:**

1. Sign up
2. Sign in

**For a logged in user:**

1. Manage Blogs / View All Blogs
2. log out.

### Using the application as a Guest

1. As a Guest User, one can **see a list of blogs** with truncated descriptions that they can scroll.
2. At the end of the truncated description, they have a **"Learn More"** link whick they can click to view the contents of the entire blog.

### Using the application as a logged in user

1. A logged in user can use the application the **same way a guest** user uses the application.
2. **In addition to this**, a logged in user can go to the **Drop-down menu->Manage Blogs** to view a list of their blogs and **create new ones**.
3. They can also **Update and Delete their blogs** from the **'Manage Blogs'** page.
4. When a User is creating or updating a blogpost, they have a **set of tools** they can use to make their blog more appealing. Nevertheless, they must **create their own content** and not paste content copied from other sources.

---

## The way forward if there was more time
1. Implement the Blog Application as a **VueJS single page app** as well.
2. Move **several front-end markup and scripts into partials** to make UI more uniform (e.g) the forms for creating and editing a post.
3. Implement **pagination for MongoDB results** and provide better Insights about other blogs.
4. Support listing of blogs by user.
5. Support **ordering of blogs** by most recent and oldest blogs and possibly between date ranges.
6. Support **User Roles** and Admin user.
7. Add Unit Test cases for some of the features.