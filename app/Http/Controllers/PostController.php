<?php

namespace App\Http\Controllers;

use App\EPost;
use App\User;

class PostController extends Controller
{
    public function getIndex() {
        $posts = EPost::all(["title", "description", "author", "oid", "updated_at", "created_at"]);
        $user_dict = User::getUserNameDictionary();
        return view('post/index', compact("posts", "user_dict"));
    }

    public function getFullPost($post_id) {
        $post = EPost::find($post_id);
        if (isset($post->description)) {
            $post->description = html_entity_decode($post->description);
        }
        $user_dict = User::getUserNameDictionary();
        return view('post/read_blog_post', compact("post", "user_dict"));
    }
}
