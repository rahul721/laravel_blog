<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\EPost;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $posts = EPost::all(); //('author', 'all', [Auth::user()->id])->get();
        // Below code was for a moment when i did not want to have a single admin who could change any blog...
        $posts = EPost::where('author', 'all', [Auth::user()->id])->get();
        // Have not thought of a better way of doing this....
        $user_dict = User::getUserNameDictionary();
        return view('home', compact("posts", "user_dict"));
    }

    public function getPostForm() {
        return view("post/create_post");
    }

    public function createPost() {
        $e_post = new EPost();
        $e_post->title = Input::get("title");
        $e_post->description = htmlentities(Input::get("description"));
        $e_post->author = Auth::user()->id;
        $e_post->save();
        return redirect()->route('home')->with('success', 'Post has been successfully added!');
    }

    public function getPost($id) {
        $post = EPost::find($id);
        if (isset($post->description)) {
            $post->description = html_entity_decode($post->description);
        }
        return view("post/view_and_manage_post", compact("post"));
    }

    public function editPost($id) {
        $post = EPost::find($id);
        if (isset($post->description)) {
            $post->description = html_entity_decode($post->description);
        }
        return view("post/edit_post", compact("post"));
    }

    public function updatePost(Request $request, $id) {
        $post = EPost::find($id);
        $post->title = $request->title;
        $post->description = htmlentities($request->description);
        // leave the original author as it is...
        $post->save();
        return redirect()->route('home')->with('success', 'Post has been updated successfully!');
    }

    public function deletePost($id) {
        $post = EPost::find($id);
        $post->delete();
        return redirect()->route('home')->with('success', 'Post has been deleted successfully!');
    }
}
